= SP-Adapter

== Конфигурация

.Описание

|===
a|Путь a|Ограничения a|Описание

a|spConfig
a|* Обязательное поле
a|Секция описания настроек генерации и отправки файлов в service-processing

a|spConfig.alwaysAttachServicePdf
a|* Устаревшее
|

a|spConfig.serviceCustomId
a|* Необязательное
a|Настройка замены кода услуги при обращении к SP.
[sidebar]
--
/v1/ORDER_CALL/**{[.line-through]#uriServiceCode#}**/{orderId}/{token}
--


a|spConfig.replacedHeaders
a|* Необязательно
a|Заголовки будут добавлены при вызове SP
Повторяющиеся заголовки заменятся


a|spConfig.files
a|* Обязательное
* Обязательно должен содержать один элемент type=request
* Может содержать любое количество других элементов
a|Описание генерируемых файлов


a|spConfig.files.type
a|* Обязательное

a|Тип или предназначение файла [pdf, commonPdf, request, xml]

* pdf - PDF-файл со стилями
* commonPdf - PDF-файл со стилями по умолчанию
* request - файл xml тело запроса SP
* xml - вложенная xml

a|spConfig.files.fileName
a|
a|

a|spConfig.files.addedFileName
a|
a|

a|spConfig.files.mnemonic
a|
a|

a|spConfig.files.addedMnemonic
a|
a|

a|spConfig.files.attachmentType
a|
a|

a|spConfig.files.templates[]
a|
a|

a|spConfig.files.templates[].Applicant
a|
a|
|===


[sidebar]
.Или можно так
--
spConfig::
alwaysAttachServicePdf:::
* Устарел
serviceCustomId:::
* Необязательное
* Настройка замены кода услуги при обращении к SP. Будет заменен
/v1/ORDER_CALL/**{[.line-through]#uriServiceCode#}**/{orderId}/{token}
--

.Пример
[source,json5]
----
{
  "spConfig": {
    "alwaysAttachServicePdf": true,
    "serviceCustomId": "pfr_ils_v3",
    "replacedHeaders": {
      "systemAuthority": "SIA=ORG_CHIEF"
    },
    "files": [
      {
        "type": "pdf",
        "fileName": "dkp.pdf",
        "addedFileName": "guid",
        "mnemonic": "dkp",
        "addedMnemonic": "guid",
        "attachmentType": "lk",
        "templates": {
            "Applicant": "dkp.vm",
        }
      }
    ],
   }
}
----

